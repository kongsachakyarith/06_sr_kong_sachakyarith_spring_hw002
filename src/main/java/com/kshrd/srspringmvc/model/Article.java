package com.kshrd.srspringmvc.model;


import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Article {
    private int id;

    @NotEmpty(message = "title cannot be empty..")
// @Size(min = 5,max = 6,message = "min is 5 max 6 ")
    private String title;

    @NotEmpty(message = "description cannot be empty..")
    private String description;


    private String profile;


    private MultipartFile file;

    public Article(int id, String title, String description, String profile) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.profile = profile;
    }

}
