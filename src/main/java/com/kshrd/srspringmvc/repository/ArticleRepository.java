package com.kshrd.srspringmvc.repository;

import com.kshrd.srspringmvc.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleRepository {
    List<Article> articles = new ArrayList<>();
   public ArticleRepository(){
       Article article1 = new Article(1,"Spring", "this is spring ", "http://localhost:8080/images/515b678d-bfa6-46cf-b797-8165e0a42c06.png");
       Article article2 = new Article(2,"Docker ", "this is Docker", "http://localhost:8080/images/7c6dba3c-1009-4a90-800c-d80c2904e67c.png");
       Article article3 = new Article(2,"Javascript", "this is Javascript", "http://localhost:8080/images/fad995d0-a53b-49c2-ae05-ee481c862f86.png");
       articles.add(article1);
       articles.add(article2);
       articles.add(article3);
    }

  public   List<Article> getAllStudent(){
        return articles;
    }
    public void addNewArticle(Article article){
        article.setId(articles.size()+1);
       articles.add(article);
    }
    public void updateArticleById(int id, Article article) {
        for (int i = 0; i < articles.size(); i++) {
            if (articles.get(i).getId() == id) {
                articles.set(i, article);
                break;
            }
        }

    }
    public void deleteArticleById(int id) {
        for (int i = 0; i < articles.size(); i++) {
            if (articles.get(i).getId() == id) {
                articles.remove(i);
                break;
            }
        }
    }
    public Article findArticleByID(int id ){
       return articles.stream().filter(article -> article.getId()== id).findFirst().orElseThrow();
    }
}
