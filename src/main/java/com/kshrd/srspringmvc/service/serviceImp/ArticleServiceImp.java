package com.kshrd.srspringmvc.service.serviceImp;

import com.kshrd.srspringmvc.model.Article;
import com.kshrd.srspringmvc.repository.ArticleRepository;
import com.kshrd.srspringmvc.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    @Override
    public List<Article> getAllArticle() {
        return articleRepository.getAllStudent();
    }

    @Override
    public Article findArticleByID(int id) {
        return articleRepository.findArticleByID(id);
    }

    @Override
    public void ArticleMethod() {
        System.out.println("Version one the article method.");
    }

    @Override
    public void AddArticleMethod(Article article) {
        articleRepository.addNewArticle(article);
    }

    @Override
    public void UpdateArticleMethod(Article article,  int id) {
        articleRepository.updateArticleById(id, article);
    }
    @Override
    public void DeleteArticleMethod(int id) {
        articleRepository.deleteArticleById(id);
    }
}
