package com.kshrd.srspringmvc.service;


import com.kshrd.srspringmvc.model.Article;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {

    public List<Article> getAllArticle();
    public Article findArticleByID(int id );
    public void ArticleMethod();
    public void AddArticleMethod(Article article);

    public void UpdateArticleMethod(Article article, int id);
    public void DeleteArticleMethod(int article);
}
