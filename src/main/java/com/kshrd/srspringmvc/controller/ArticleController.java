package com.kshrd.srspringmvc.controller;

import com.kshrd.srspringmvc.model.Article;
import com.kshrd.srspringmvc.service.FileStorageService;
import com.kshrd.srspringmvc.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class ArticleController {

    @Autowired
  @Qualifier("articleServiceImp")

    ArticleService articleService;
    // 3 methods : field, setter, constructor

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles", articleService.getAllArticle());
        return "index";


    }

    @GetMapping("/form-add")
    public String showFormAdd(Model model){

        model.addAttribute("article",new Article());
        return "form-add";
    }
    @GetMapping("/update-article/{id}")
    public String showFormUpdate(@PathVariable int id,Model model){
        model.addAttribute("article", articleService.findArticleByID(id));
        return "form-update";
    }
//
//    @RequestParam("file") MultipartFile file,
    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Article article, BindingResult bindingResult){

        if (article.getFile().isEmpty()){
            article.setProfile("http://localhost:8080/images/53563e29-4d26-4a4d-98ae-d3c02c992870.jpg");
        }
        else {
            try{
                String filename = "http://localhost:8080/images/"+fileStorageService.saveFile(article.getFile());
                article.setProfile(filename);

            }catch (IOException ex){
                System.out.println("Error with the imaage upload "+ex.getMessage());
            }
        }

        if (bindingResult.hasErrors()){
            return "/form-add";
        }
        articleService.AddArticleMethod(article);

      return  "redirect:/";
    }
    @GetMapping("/view-article/{id}")
    public String viewStudent(@PathVariable int id,Model model){
        Article resultArticle = articleService.findArticleByID(id);
        model.addAttribute("article", resultArticle);


        return "form-view";
    }

    @PostMapping("/handle-update/{id}")
    public String handleUpdate(@PathVariable int id, @ModelAttribute @Valid Article article, BindingResult bindingResult){
        Article resultArticle = articleService.findArticleByID(id);
        if(article.getFile().isEmpty()){
            article.setProfile(resultArticle.getProfile());
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(article.getFile());
                System.out.println("filename: " + filename);
                System.out.println(article);
                article.setProfile(filename);
            }catch (Exception e){
                System.out.println("Error with the images upload" + e.getMessage());
            }
        }
        articleService.UpdateArticleMethod(article, id);

        return "redirect:/";
    }
    @GetMapping("/delete-article/{id}")
    public String deleteArticle(@PathVariable int id, Model model){
        articleService.DeleteArticleMethod(id);
        return "redirect:/";
    }

}
